package com.mytaxi.android_demo.common;

import android.view.View;

import com.mytaxi.android_demo.R;
import com.mytaxi.android_demo.utils.HttpUtils;

import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class Functions {

    public void loginToApp() {
//        Fetches credentials from APIs (Instead of putting hard code, directly fetch from API response)
        String userName = HttpUtils.fetchCredentials().get("username");
        String password = HttpUtils.fetchCredentials().get("password");
        final Matcher<View> loginButtonMatcher = withId(R.id.btn_login);

//        Wait for 5 seconds to launch app
        waitFor(5);

//        Enter credentials in edit text
        enterUserNameAndPassword(userName, password);

//        Click on login button
        clickOnButton(loginButtonMatcher);
    }

    public void waitFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void enterUserNameAndPassword(String userName, String password) {
        final Matcher<View> userNameEditTextMatcher = withId(R.id.edt_username);
        fillEditText(userNameEditTextMatcher, userName);
        final Matcher<View> passwordEditTextMatcher = withId(R.id.edt_password);
        fillEditText(passwordEditTextMatcher, password);
    }

    public void fillEditText(Matcher<View> editTextMatcher, String value) {
        onView(
                editTextMatcher)
                .perform(typeText(value), closeSoftKeyboard());
    }

    public void clickOnButton(Matcher<View> buttonMatcher) {
        onView(
                buttonMatcher)
                .perform(click());
    }
}
