package com.mytaxi.android_demo.utils;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.mytaxi.android_demo.misc.Constants.SOCKET_TIMEOUT;

public class HttpUtils {
    private static final OkHttpClient mClient = new OkHttpClient.Builder().readTimeout(SOCKET_TIMEOUT, TimeUnit.SECONDS).build();
    private static final JsonParser mJsonParser = new JsonParser();
    private static String RANDOM_USER_URL = "https://randomuser.me/api/";
    private static String seed = "a1f30d446f820665";
    private static String url = RANDOM_USER_URL + "?seed=" + seed;
    private static Request request = new Request.Builder().url(url).build();
    private static HashMap<String, String> credentialMap = new HashMap<>();

    public static HashMap<String, String> fetchCredentials() {
        try {
            Response jsonResponse = mClient.newCall(request).execute();
            JsonObject jsonObject = mJsonParser.parse(jsonResponse.body().string().trim()).getAsJsonObject();
            JsonArray results = jsonObject.getAsJsonArray("results");
            JsonElement jsonElement = results.get(0);
            JsonObject jsonUser = jsonElement.getAsJsonObject();
            JsonObject login = jsonUser.getAsJsonObject("login");
            credentialMap.put("username", login.get("username").getAsString());
            credentialMap.put("password", login.get("password").getAsString());
            return credentialMap;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
