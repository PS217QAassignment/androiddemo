package com.mytaxi.android_demo;


import android.content.Intent;
import android.net.Uri;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;

import com.mytaxi.android_demo.common.Functions;

import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class DriverTest extends BaseTest {

    private Functions functions = new Functions();



    @Test
    public void driverTest() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html

//        Initialized all matcher

        final Matcher<View> textSearchMatcher = withId(R.id.textSearch);
        final Matcher<View> callToDriverMatcher = withId(R.id.fab);
        final Matcher<View> driverNameMatcher = withText("Sarah Scott");
        final Matcher<View> selectedDriverNameMatcher = withId(R.id.textViewDriverName);
        final Uri INTENT_DATA = Uri.parse("tel:" + "413-868-2228");

        String driverNameStartsWith = "Sa";

        functions.loginToApp();

//        Wait for 3 seconds to get new screen
        functions.waitFor(3);

//        Enter initials for driver name and click on selected driver
        selectDriverNameFromList(textSearchMatcher, driverNameMatcher, driverNameStartsWith);

//        Validate text for selected driver
        checkDriverName(driverNameMatcher, selectedDriverNameMatcher);

//         Click on call to driver button
        functions.clickOnButton(callToDriverMatcher);

//          Check intent of called driver
        intended(allOf(hasAction(Intent.ACTION_DIAL), hasData(INTENT_DATA)));
    }


    private void checkDriverName(Matcher<View> driverNameMatcher, Matcher<View> selectedDriverNameMatcher) {
        onView(selectedDriverNameMatcher)
                .check(matches(driverNameMatcher));
    }

    private void selectDriverNameFromList(Matcher<View> textSearchMatcher, Matcher<View> driverNameMatcher, String driverNameStartsWith) {

        functions.fillEditText(textSearchMatcher, driverNameStartsWith);

        onView(driverNameMatcher)
                .inRoot(withDecorView(not(is(mActivity.getWindow().getDecorView()))))
                .perform(click());
    }
}
